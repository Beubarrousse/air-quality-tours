<?php
/**
 * Created by PhpStorm.
 * User: arthurLEFEBVRE
 * Date: 19/01/2019
 * Time: 11:28
 */

require('models/SalleManager.php');
class SalleController
{
    private $salleManager;

    public function __construct()
    {
        $this->salleManager = new SalleManager();

    }

    public function show(){
        $salles = $this->salleManager->getAll();

        require('views/accueil.php');
    }
}