<?php

include('header.php');
require('controllers/SalleController.php');
$salleController = new SalleController();

if(isset($_GET['p'])){
    if($_GET['p'] == 'accueil'){
        $salleController->show();
    }
    if($_GET['p'] == 'graphiques'){
        require('views/indicateurs/graphiques.php');
    }
}else{
    $salleController->show();
}


?>

