<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Air-quality Tours</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav ">
            <li class="nav-item">
                <a class="nav-link <?php if($_GET['p'] == 'accueil' || !isset($_GET['p'])) echo 'active"'; ?>" href="index.php?p=accueil">Mesures en cours <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle <?php if($_GET['p'] == 'graphiques') echo 'active"'; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Indicateurs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?p=graphiques">Graphiques</a>
                    <a class="dropdown-item" href="#">Historique</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ">
            <li class="nav-item <?php if($_GET['p'] == 'administration') echo 'active"'; ?>">
                <a class="nav-link" href="#">Administration</a>
            </li>
        </ul>
    </div>
</nav>