<div class="jumbotron text-center">
    <h1>Polytech'Tours</h1>
    <p>Mesure de la qualité de l'air au sein du bâtiment Portalis</p>
</div>

<div class="container-fluid">
    <h3>Tableau des mesures en cours : </h3>
    <br>
    <table class="table">
        <thead class="bg-light">
        <tr>
            <th scope="col">Nom salle</th>
            <th scope="col">Pression</th>
            <th scope="col">Humidité</th>
            <th scope="col">Température</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($salles as $salle){ ?>
                <tr>
                    <td scope="row"><?php echo $salle['intitule'] ?></td>
                    <td><?php echo $salle['pression'] ?> hPa</td>
                    <td><?php echo $salle['humidite'] ?> %</td>
                    <td><?php echo $salle['temperature'] ?> °C</td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
</div>