<style>
#chartdiv {
width: 060%;
  height: 500px;
}
</style>

<!-- Chart code -->
<script>
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "none",
    "dataProvider": [{
    "date": "2009-06-02",
        "value": 5
    }, {
    "date": "2009-06-03",
        "value": 15
    }, {
    "date": "2009-06-04",
        "value": 13
    }, {
    "date": "2009-06-05",
        "value": 17
    }, {
    "date": "2009-06-06",
        "value": 15
    }, {
    "date": "2009-06-09",
        "value": 19
    }, {
    "date": "2009-06-10",
        "value": 21
    }, {
    "date": "2009-06-11",
        "value": 20
    }, {
    "date": "2009-06-12",
        "value": 20
    }, {
    "date": "2009-06-13",
        "value": 19
    }, {
    "date": "2009-06-16",
        "value": 25
    }, {
    "date": "2009-06-17",
        "value": 24
    }, {
    "date": "2009-06-18",
        "value": 26
    }, {
    "date": "2009-06-19",
        "value": 27
    }, {
    "date": "2009-06-20",
        "value": 25
    }, {
    "date": "2009-06-23",
        "value": 29
    }, {
    "date": "2009-06-24",
        "value": 28
    }, {
    "date": "2009-06-25",
        "value": 30
    }, {
    "date": "2009-06-26",
        "value": 39,
        "customBullet": "https://www.amcharts.com/lib/3/images/redstar.png"
    }, {
    "date": "2009-06-27",
        "value": 29
    }, {
    "date": "2009-06-30",
        "value": 31
    }],
    "valueAxes": [{
    "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "graphs": [{
    "bulletSize": 14,
        "customBullet": "https://www.amcharts.com/lib/3/images/star.png?x",
        "customBulletField": "customBullet",
        "valueField": "value",
         "balloonText":"<div style='margin:06px; text-align:left;'><span style='font-size:13px'>[[category]]</span><br><span style='font-size:18px'>Value:[[value]]</span>",
    }],
    "marginTop": 20,
    "marginRight": 70,
    "marginLeft": 40,
    "marginBottom": 20,
    "chartCursor": {
    "graphBulletSize": 1.5,
     	"zoomable":false,
      	"valueZoomable":true,
         "cursorAlpha":0,
         "valueLineEnabled":true,
         "valueLineBalloonEnabled":true,
         "valueLineAlpha":0.2
    },
    "autoMargins": false,
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "date",
    "valueScrollbar":{
    "offset":30
    },
    "categoryAxis": {
    "parseDates": true,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
    "enabled": true
    }
});
</script>

<!-- HTML -->
<div class="container-fluid">
    <h3>Evolution de la température moyenne dans le bâtiment Portalis sur le mois de Juin : </h3>
    <br>
    <div id="chartdiv" style="margin: auto;"></div>
</div>