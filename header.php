<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="IE=8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="fr-FR" />

<!--    Imports Bootstrap -->
<!--    Javascript -->
    <script type="text/javascript" src="resources/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="resources/scripts/Popper.js"></script>
    <script type="text/javascript" src="resources/scripts/bootstrap.min.js"></script>
<!--    CSS -->
    <link rel="stylesheet" href="resources/styles/bootstrap.min.css"/>

<!--    Imports AmCharts -->
    <script type="text/javascript" src="resources/plugins/amcharts/amcharts.js"></script>
    <script type="text/javascript" src="resources/plugins/amcharts/serial.js"></script>



    <title>Polytech Tours</title>
</head>
<?php include('views/navbar.php'); ?>
