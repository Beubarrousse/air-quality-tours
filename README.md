**Ecole Polytechnique de l'Université de Tours**

Projet Smart Systems : mesure de la qualité de l'air au sein d'une infrastructure

Binôme : Arthur LEFEBVRE, Pierre CASSEGRAIN

Encadrant : M.GAUCHER

Création d'une interface utilisateur web afin de pouvoir visionner et exploiter en temps réel les données concernant la qualité de l'air au sein du bâtiment Portalis.