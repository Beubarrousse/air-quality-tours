<?php
/**
 * Created by PhpStorm.
 * User: arthurLEFEBVRE
 * Date: 19/01/2019
 * Time: 11:28
 */

class SalleManager
{
    // Connexion à la base de données MySql
    private function connexionBDD(){
        // Connexion à la base de données Qubes
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=air-quality', 'root', '',
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        }
        catch(Exception $e)
        {
            echo 'Erreur : '.$e->getMessage().'<br />';
            echo 'N° : '.$e->getCode();
            die();
        }

        return $bdd;
    }


    public function create(){

    }

    public function getAll(): array {
        $bdd = $this->connexionBDD();
        $salles = array();

        $query = $bdd->query('SELECT *
                                       FROM salle');
        $query->execute();

        while($donnees = $query->fetch()){
            $salles[] = $donnees;
        }

        return $salles;
    }

    public function get(int $id){

    }


    public function update(){

    }

    public function destroy(){

    }
}